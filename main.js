// 1) Метод об'єкта це функція яка належить об'єкту.
// Конструктор Object створює об'єкт-обгортку для переданого значення.
// Якщо значенням є null або undefined, створює і повертає порожній об'єкт,
// в іншому випадку повертає об'єкт такого типу, який відповідає переданому значенням.
// 2)Властивість є значення або набір значень (у вигляді масиву або об'єкта),
// який є членом об'єкта і може містити будь який тип даних.
// 3)Типи посилань — це об'єкти, включаючи Object, і Function.
// Зважаючи на те, що ці типи можуть містити дуже великі обсяги дуже різнорідних даних, змінна,
//  що містить тип посилань, фактично його значення не містить.
//   Вона містить посилання місце у пам'яті, де розміщуються реальні дані.


function creatNewUser() {
  const firstName = prompt("Enter your name");
  const lastName = prompt("Enter your last name");

  return {
    _firstName: firstName,
    _lastName: lastName,

    getLogin() {
      return (this._firstName[0] + this._lastName).toLowerCase();
    },

    setFirstName(value) {
      this._firstName = value;
    },
    setLastName(value) {
      this._lastName = value;
    },
  };
}

const newUser= creatNewUser()
console.log(newUser);
console.log(newUser.getLogin())